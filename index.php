<!DOCTYPE html>
<html>
<head>
    <meta charset="ISO 8859-1"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Arriendo de Canchas</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" type="text/css" media="screen" href="css/main.css" />
</head>
<body>
    <table>
        <tr>
            <!--Columna 1 contiene formulario-->
            <td>
                <form>
                    <table>
                        <thead>
                            <tr>
                                <th colspan="2"><h2 class="titulo_ppal">ARRENDAR CANCHA</h2></th>
                            </tr>
                            <tr>
                                <td colspan="2"><h3 class="titulo_sec" >Reserva tu cancha</h3></td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><label for="nombre">Nombre y apellido</label></td>
                                <td><label for="rut">RUT</label></td>
                            </tr>
                            <tr>
                                <td><input type="text" name="nombre" id="nombre" onkeypress="return soloLetrasNombre(event)"/></td>
                                <td><input type="text" name="rut" id="rut" onkeypress="return soloNumerosRut(event)"/></td>
                            </tr>
                            <tr>
                                <td><label for="email">Email</label></td>
                                <td><label for="fono">Fono</label></td>
                            </tr>
                            <tr>
                                <td><input type="email" name="email" id="email" onkeypress="return ingresoCorreoElectronico(event)"/></td>
                                <td><input type="tel" name="fono" id="fono" onkeypress="return soloNumerosEnterosTelefono(event)"/></td>
                            </tr>
                            <tr>
                                <td><label for="fechaArriendo">Fecha a arrendar</label></td>
                                <td><label for="horaArriendo">Hora a arrendar</label></td>
                            </tr>
                            <tr>
                                <td><input type="date" name="fechaArriendo" id="fechaArriendo" /></td>
                                <td><input type="time" name="horaArriendo" id="horaArriendo" /></td>
                            </tr>
                            <tr>
                                <td><label for="cancha">Cancha</label></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>
                                    <select name="cancha" id="cancha">                                    
                                    </select>
                                </td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table><br>
                    <table>
                        <tr>
                            <td>
                                <label id="customEntero">Como se enteró de Nosotros:&nbsp&nbsp</label>
                            </td>     
                            <td>
                                <input type="checkbox" name="web" id="web"><label class="formulario-check"> Web&nbsp&nbsp</label>
                            </td>
                            <td>
                                <input type="checkbox" name="rs" id="rs"><label class="formulario-check"> Redes Sociales&nbsp&nbsp</label><br>
                            </td>          
                        </tr>
                        <tr>
                            <td></td> <!--Para generar el escalado de la tabla...--->
                            <td>
                                <input type="checkbox" name="TV" id="TV"><label class="formulario-check"> TV&nbsp&nbsp</label>
                            </td>
                            <td>
                                <input type="checkbox" name="amigo" id="amigo"><label class="formulario-check"> Amigo</label>  
                            </td>
                        </tr>
                        <th colspan="3">
                            <input type="button" class="botonArriendo" value="Arrendar" onclick="return operacion()">
                        </th>
                    </table> 
                </form>
            </td>
            <!--Columna 2 contiene la tabla Resumen de arriendos-->
            <td id="resumenCancha"><br><br>
                <h4 class="tituloResumenArriendos">Resumen de Arriendos</h6>
                <h6>Totalizado de arriendos por cancha</h6>
                <table id="tablaResumenArriendos">
                    <thead>
                        <tr><th colspan="3"><label>Total de Canchas</label></th></tr>
                        <tr>
                            <td><label>#</label></td>
                            <td><label>Nombre Cancha</label></td>
                            <td><label>% de Arriendo</label></td>
                        </tr>
                    </thead>
                    <tbody class="canchaEstadistica" id="canchaEstadistica">
                    <!-- Autocompletado de la estadística-->
                    </tbody>
                </table>
            </td>
        </tr>
    </table>
    <!--Tabla canchas arrendadas-->
    <h4 class="tituloCanchasArrandadas">Listado de Canchas Arrendadas</h4>
    <table id="tablaResumenCancha">
        <thead>
            <tr>
                <td><label>Nombre</label></td>
                <td><label>Arrendada por</label></td>
                <td><label>Fecha Agendada</label></td>
                <td><label>Acciones</label></td>
            </tr>
        </thead>
        <tbody class="canchaArrendada" id="canchaArrendada">   
        <!--Autocompletado de las canchas arrendadas -->       
        </tbody>
    </table>
</body>
<footer>
    <script src="js/Controlador.js"></script>
    <script src="js/utils.js"></script>
</footer>
</html>