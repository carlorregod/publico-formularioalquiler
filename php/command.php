<?php
require 'conexion.php';

$cmd=$_POST['cmd'];

switch($cmd)
{
    case 'cmbCancha':      
        $pgsql=getConnect();
        $query="SELECT * FROM canchas";
        $consulta=$pgsql->prepare($query);
        $consulta->execute();
        $listas='<option value="0" id="0">Seleccionar opci�n</option>';
        while ($fila = $consulta->fetchObject())
        {
            $listas .='<option value="'.$fila->id.'" id="'.$fila->id.'">'.$fila->nombre_cancha.'</option>';
        } 
        echo $listas;
        break;

    case 'lstCancha':
        $pgsql=getConnect();
        $query="SELECT id, nombre, cancha, fecha, hora FROM reserva_canchas";
        $consulta=$pgsql->prepare($query);
        $consulta->execute();
        $listas='';
        $i=1; //�til para dar un value ascendente a las filas
        while ($fila = $consulta->fetchObject())
        {
            $listas .='<tr id="'
            .$fila->id.
            '"><td>'
            .$fila->cancha.
            '</td><td>'
            .$fila->nombre.
            '</td><td>'
            .date("d/m/Y", strtotime($fila->fecha)).' '.date("h:m", strtotime($fila->hora)).
            '</td><td><button value="'
            .$i.
            '" id="'
            .$fila->id.
            '" onclick="return borra(this)">Borrar</button></td></tr>';
            $i++;
        } 
        echo $listas;
        break;

        case 'estCancha':
            $pgsql=getConnect();
            //SE DEBE CORREGIR, PORQUE LO QUE SE PRECISA ES LA CANCHA CON LAS ESTADISTICAS
            $query="select * from vw_estadistica_canchas";
            $consulta=$pgsql->prepare($query);
            $consulta->execute();
            $listas='';
            while ($fila = $consulta->fetchObject())
            {
                $listas .='<tr><td>'
                .$fila->id.
                '</td><td>'
                .$fila->cancha.
                '</td><td>'
                .$fila->porcentaje_arriendo.
                ' %</td></tr>';
            } 
            echo $listas;
            break;
        
    case 'EnviarDatos':
        /*Traslado de datos empaquetados*/
        $nombre=utf8_decode(pg_escape_string($_POST['nombre']));
        $rut=utf8_decode(pg_escape_string($_POST['rut']));
        $email=utf8_decode(pg_escape_string($_POST['email']));
        $fono=utf8_decode(pg_escape_string($_POST['fono']));
        $fechaArriendo=utf8_decode($_POST['fechaArriendo']);
        $horaArriendo=utf8_decode($_POST['horaArriendo']);
        $cancha=utf8_decode($_POST['cancha']);
        $web=$_POST['web'];
        $rs=$_POST['rs'];
        $TV=$_POST['TV'];
        $amigo=$_POST['amigo'];
        //Formateo previo tel�fono
        $fono=trim(str_replace('+','',$fono));
        /*Trabajando con la DB*/      
        $pgsql=getConnect();
        /*La query se debe formar de las caracter�sticas del insert en query/query.pgsql. Pero debido a
        que las canchas est�n en una tabla aparte, se debe averiguar el valor de ese campo */
        $query="SELECT nombre_cancha FROM canchas WHERE id='$cancha'";
        $q_cancha= $pgsql->prepare($query);
        $q_cancha->execute();
        /*Con ello, se rescata el valor de la cancha del ID que corresponde*/
        $cancha_consultada=$q_cancha->fetchObject()->nombre_cancha; //La query s�lo tendr� una ocurrencia
        /* No se ejecutar� una acci�n si la cancha ya se encuentra reservada a una misma fecha-hora */
        $query1="SELECT * FROM reserva_canchas WHERE fecha='$fechaArriendo' AND hora='$horaArriendo' AND cancha='$cancha_consultada'";
        $c_cancha=$pgsql->prepare($query1);
        $c_cancha->execute();
        //Si existe al menos un registro, esto har� break ac�
        $CountReg=$c_cancha->fetchAll();
        count($CountReg);
        if(count($CountReg) >1)
        {
            echo 'error_';
            break; 
        }
        else
        {
            try
            {
                $query="SELECT fn_ingresoFormulario_i('$nombre', '$rut', '$email', '$fono', '$fechaArriendo', '$horaArriendo', '$cancha_consultada', '$web', '$TV', '$rs', '$amigo')";
                $consulta=$pgsql->query($query);
                $respuesta=$consulta->fetchAll()[0][0];
                //Ya ejecutado se puede salir del select y ver resultados
                echo $respuesta; 
                break; 
            }
            catch(Exception $e)
            {
                echo '7';
                break;
            }
            
        }     

    case 'BorrarRegistro':
        //Recolectando variables
        $id_boton=$_POST['id_boton'];
        //Lueog de iniciar la sesi�n, se procede al borrado
        $pgsql=getConnect();
        $query="SELECT fn_eliminarregistro_d('$id_boton')";
        $q_cancha= $pgsql->prepare($query);
        $q_cancha->execute();
        echo '';
        break;

    default:
        break;
}