function soloLetrasNombre(e) 
{
   key = e.keyCode || e.which;
   tecla = String.fromCharCode(key).toLowerCase();
   letras = "�����abcdefghijklmn�opqrstuvwxyz ";  //notar que no dejar� ingreso de espacios. S�lo caracteres
   //especiales = [8,37,39,46];

   tecla_especial = false;

	if(letras.indexOf(tecla)==-1 && !tecla_especial)
	{
		return false;
	}
}

function ingresoCorreoElectronico(e)
{
   key = e.keyCode || e.which;
   tecla = String.fromCharCode(key).toLowerCase();
   letras = "�����abcdefghijklmn�opqrstuvwxyz@._-0123456789"; 

   tecla_especial = false;

	if(letras.indexOf(tecla)==-1 && !tecla_especial)
	{
		return false;
	}
}

function soloNumerosRut(e)
{
    //S�lo acepta n�meros positivos y decimales
	tecla = (document.all) ? e.keyCode : e.which; 
	if (tecla==8) return true; 
	patron =/^[0-9\-kK]+$/;//este acepta giones y letra k, si se quiere eliminar borrar el punto despues del 9. 
	te = String.fromCharCode(tecla); 
	return patron.test(te);
}

function soloNumerosEnterosTelefono(e)
{
    //Solo n�meros enteros 
	tecla = (document.all) ? e.keyCode : e.which; 
	if (tecla==8) return true; 
	patron =/^\d+$|^\++$/;
	te = String.fromCharCode(tecla); 
	return patron.test(te);
}