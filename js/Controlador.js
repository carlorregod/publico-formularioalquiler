/*PARTE 1: LLENANDO DIN�MICAMENTE LOS COMBOBOX DEL FORMULARIO */
//En cuanto cargue la p�gina efectuar� esto:

window.onload = function()
{
    cargaLista();
    return false;
}

function cargaLista()
{
    /* FORMACION DEL CHECKBOX */
    params = '&cmd=cmbCancha';
     //AJAX
     var xhttp = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
     xhttp.onreadystatechange = function() {
         if (this.readyState == 4 && this.status == 200) 
         {  //El servidor procesa la solicitud.
            document.getElementById('cancha').innerHTML = this.responseText;  //Respuesta 1: El checkbox
         }
     };
     xhttp.open("POST", "php/command.php", true);    // Tipo de comunicacion
     xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
     xhttp.send(params);  // envio de http

    /*FORMACION DEL RESUMEN DE CANCHAS */
    params = '&cmd=lstCancha';
    //AJAX
    var xhttp = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) 
        {  //El servidor procesa la solicitud.
        document.getElementById('canchaArrendada').innerHTML = this.responseText;  //Respuesta 2: Lista de la cancha
        }
    };
    xhttp.open("POST", "php/command.php", true);    // Tipo de comunicacion
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send(params);  // envio de http

    /*FORMACI�N DE LA ESTAD�STICA DE CANCHAS ARRENDADAS*/
    params = '&cmd=estCancha';
    //AJAX
    var xhttp = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) 
        {  //El servidor procesa la solicitud.
        document.getElementById('canchaEstadistica').innerHTML = this.responseText;  //Respuesta 3: Estad�stica canchas
        }
    };
    xhttp.open("POST", "php/command.php", true);    // Tipo de comunicacion
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send(params);  // envio de http

    return false;
}

/*PARTE 2: M�TODOS Y FUNCIONES VARIAS*/
//Revisi�n de ingresos Nombre, RUT y email. Si aceptan las condiciones espec�ficas:

//Para validar rut tipo chileno (ejemplo: 1111111-1)
function revisaRut(rut)
{
    // Despejar Puntos
    var valor = rut.replace('.','');
    // Despejar Gui�n
    valor = valor.replace('-','');
    // Aislar Cuerpo y D�gito Verificador
    cuerpo = valor.slice(0,-1);
    dv = valor.slice(-1).toUpperCase();
    // Formatear RUN
    rut.value = cuerpo + '-'+ dv
    // Si no cumple con el m�nimo ej. (n.nnn.nnn)
    if(cuerpo.length < 7)
    {
        return 1; //Equivale a RUT inv�lido por ingreso
    }
    // Calcular d�gito verificador
    suma = 0;
    multiplo = 2;
    // Para cada d�gito del Cuerpo
    for(i=1;i<=cuerpo.length;i++)
   {
        // Obtener su Producto con el M�ltiplo Correspondiente
        index = multiplo * valor.charAt(cuerpo.length - i);
        // Sumar al Contador General
        suma = suma + index;
        // Consolidar M�ltiplo dentro del rango [2,7]
        if(multiplo < 7) { multiplo = multiplo + 1; } else { multiplo = 2; }
    }

    // Calcular D�gito Verificador en base al M�dulo 11
    dvEsperado = 11 - (suma % 11);

    // Casos Especiales (0 y K)
    dv = (dv == 'K' || dv == 'k')?10:dv;
    dv = (dv == 0)?11:dv;

    // Validar que el Cuerpo coincide con su D�gito Verificador
    if(dvEsperado != dv) 
        return 2; //nEquivale a RUT inv�lido por nv invalido
    //Retorno al d�gito verificador
    dv = (dv == 10)?'K':dv;
    dv = (dv == 11)?0:dv;
    document.getElementById('rut').value = cuerpo+"-"+dv;
    return 0; //Equivale a que el RUT es v�lido
}

//Validar� el email
function revisaEmail(email)
{
    //Variable regex permitir� almacenar criterio de validaci�n: nombre1@ejemplo.com.
    var regex = /^([a-zA-Z])([\w.-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email) ? true : false;
}

//Validar� el nombre y apellido ingresado. M�nimo dos palabras separadas por un espacio
function revisaNombreApellido(nombre)
{
    //Variable regex permitir� almacenar criterio de validaci�n: nombre1@ejemplo.com.
    var regex = /^([a-zA-Z������])+([a-zA-Z������\ ])+$/;
    return regex.test(nombre) ? true : false;
}

//Validar� el formato del n�mero de tel�fono
function revisaFono(numero)
{
    //Variable regex permitir� medir la estructura de un n�mero para que sea ingresado como n�mero de tel�fono.
    var regex = /(^[+][1-9]{1}[0-9]{8,11}$)|(^[1-9]{1}[0-9]{6,12}$)/;  //Para n�meros del tipo +224569876, +5692365893 o 5698745 de 7 a 10 d�gitos
    //Revisando la forma de tipolog�a del n�mero
    var esRegex=regex.test(numero) ? true : false;
    //Notar que el signo '+' no podr� ser ingresado desde el formulario
    var long=(numero.length>=7 && numero.length<=12) ? true : false;
    //Retorno 1: El n�mero no cumple las longitudes m�nimas
    if(!long)
        return 1;
    if(!esRegex)
        return 2;
    else
        return 0;     
}

//Validando los checkbox. M�nimo 2 selecciones para pasar
function revisaCheckbox(cb1,cb2,cb3,cb4)
{
    var contador=0;
    if(cb1)
        contador++;
    if(cb2)
        contador++;
    if(cb3)
        contador++;
    if(cb4)
        contador++;
    //Los retornos depender�n si el contador es de 2 o m�s.
    if(contador>=2)
        return true;
    else
        return false;
}
/*FIN PARTE 2*/

/*LAS OEPRACIONES POR BOT�N: PARTE 3 */
function operacion()
{
    //Recolecci�n de variables desde el HTML
    var nombre=document.getElementById('nombre').value;
    var rut=document.getElementById('rut').value;
    var email=document.getElementById('email').value;
    var fono=document.getElementById('fono').value;
    var fechaArriendo=document.getElementById('fechaArriendo').value;
    var horaArriendo=document.getElementById('horaArriendo').value;
    var cancha=document.getElementById('cancha').value;

    //Recolecci�n desde los checkbox
    var web=document.getElementById('web').checked;
    var rs=document.getElementById('rs').checked;
    var TV=document.getElementById('TV').checked;
    var amigo=document.getElementById('amigo').checked;
    /*  CONJUNTO DE REVISIONES ANTES DE ENVIAR DATOS */

    //Validaciones de campos vac�os
    if(nombre =='' || rut =='' || email =='' || fono =='' || fechaArriendo=='' || horaArriendo== '' || cancha== 0)
	{
        alert('Debe completar todos los campos de este formulario.');
        return false;
    }

    //Validando que el nombre y apellido sea ingresado
    if(!revisaNombreApellido(nombre))
    {
    alert('Debe seleccionar al menos su nombre y apellido. Revise de no tener espacios al inicio del ingreso.');
    return false;
    }

    //Chequear que el RUT sea el v�lido
    var checkrut=revisaRut(rut);
    if (checkrut ==2)
    {
        alert("RUT no v�lido. Favor corregir ingreso y/o d�gito verificador de tipo 1111111-1");
        return false;
    }
    else if(checkrut == 1)
    {
        alert("RUT no v�lido. Favor corregir a la forma tipo 1111111-1");
        return false;
    }
    else if(checkrut == 0)
    {
        rut=document.getElementById('rut').value;
    }
    else
    {
        alert('Error.');
    }

    //Efectuar validaci�n de correo electr�nico
    if(!revisaEmail(email))
    {
        alert('Ingrese un correo de forma correcta con formato alguien1@ejemplo.cl.');
        return false;
    }
 
    var validaFono=revisaFono(fono);
    if(validaFono == 1)
    {
        alert('Debe ingresar un n�mero de tel�fono entre 7 y 13 caracteres. Anteponer el caracter + en el tel�fono es opcional (ejemplo: 56986547475.)');
        return false;
    }
    else if(validaFono == 2)
    {
        alert('Corrija los formatos de n�mero de tel�fono ingresados a la forma +9999999999 o 999999999.)');
        return false;
    }

    //Revisiones de checkbox
    if(!revisaCheckbox(web,rs,TV,amigo))
    {
        alert('Debe seleccionar al menos 2 opciones de las alternativas de c�mo se enter� de nosotros.');
        return false;
    }

    var nombre=document.getElementById('nombre').value;
    var rut=document.getElementById('rut').value;
    var email=document.getElementById('email').value;
    var fono=String(document.getElementById('fono').value);
    var fechaArriendo=document.getElementById('fechaArriendo').value;
    var horaArriendo=document.getElementById('horaArriendo').value;
    var cancha=document.getElementById('cancha').value;

    //Recolecci�n desde los checkbox
    var web=document.getElementById('web').checked;
    var rs=document.getElementById('rs').checked;
    var TV=document.getElementById('TV').checked;
    var amigo=document.getElementById('amigo').checked;

    //Empaquetamiento de variables
    parametro = '&cmd=EnviarDatos'
    +'&nombre=' + nombre
    +'&rut=' + rut
    +'&email=' + email
    +'&fono=' + fono
    +'&fechaArriendo=' + fechaArriendo
    +'&horaArriendo=' + horaArriendo
    +'&cancha=' + cancha
    +'&web=' + web
    +'&rs=' + rs
    +'&TV=' + TV
    +'&amigo=' + amigo;
    
    // AJAX
    var xhttp = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
    xhttp.onreadystatechange = function() {
        if(this.readyState == 4 && this.status == 200)      // El servidor procesa la solicitud.
            if(this.responseText == 'error_')
                alert('La cancha seleccionada ya est� reservada para esa fecha y hora. Favor seleccione otra o la misma en distinta fecha/hora.');
            else
                {
                    //Switch que controla la salida del AJAX:
                    switch(this.responseText)
                    {
                        case '0': //Caso de �xito
                            alert('Datos a�adidos exitosamente');
                            console.log(this.responseText); 
                            cargaLista();
                            document.getElementById('nombre').value='';
                            document.getElementById('rut').value='';
                            document.getElementById('email').value='';
                            document.getElementById('fono').value='';
                            document.getElementById('fechaArriendo').value='';
                            document.getElementById('horaArriendo').value='';
                            document.getElementById('cancha').value=0;
                            document.getElementById('web').checked=false;
                            document.getElementById('rs').checked=false;
                            document.getElementById('TV').checked=false;
                            document.getElementById('amigo').checked=false; 
                            break;

                        case '-1': //Error por espacios en blanco
                            alert('Debe completar todos los campos del formulario.');
                            console.log(this.responseText); 
                            break;

                        case '1': //Error por nombre
                            alert('Debe seleccionar al menos su nombre y apellido. Revise de no tener espacios al inicio del ingreso. ');
                            console.log(this.responseText); 
                            break;

                        case '2': //Error por rut
                            alert('RUT no v�lido. Favor corregir ingreso y/o d�gito verificador de tipo 1111111-1.');
                            console.log(this.responseText); 
                            break;

                        case '3': //Error por email
                            alert('Ingrese un correo de forma correcta con formato alguien1@ejemplo.cl.');
                            console.log(this.responseText); 
                            break;

                        case '4': //Error por tel�fono
                            alert('Ingrese n�mero de tel�fono v�lido de la forma +5698789878 o 5647895.');
                            console.log(this.responseText); 
                            break;

                        case '5': //Error por cancha con tope de horario
                            alert('La cancha seleccionada ya est� reservada para esa fecha y hora. Favor seleccione otra o la misma en distinta fecha/hora.');
                            console.log(this.responseText); 
                            break;

                        case '6': //Error por no seleccionar m�s de 1 casilla
                            alert('Debe seleccionar al menos 2 opciones de las alternativas de c�mo se enter� de nosotros.');
                            console.log(this.responseText); 
                            break;

                        case '7': //Error por catch de base de datos
                            alert('Error. Int�ntelo m�s adelante.');
                            console.log(this.responseText); 
                            break; 

                        default: //Error por defecto
                            alert('Error del sistema, int�ntelo m�s adelante.');
                            break;
                    }
     
                }           
        else
            console.log('Si ve este mensaje m�s de 3 veces por env�o, hay un error');
    };
    xhttp.open("POST", "php/command.php", true);    // Tipo de comunicacion
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send(parametro);  // envio de http

    /*Finalziado todo, se debe reiniciar el formulario y se actualiza la lista de reservas*/
         
    return false;
    }
/*Fin operaciones por bot�n */

/*PARTE 4: ACCI�N DE DELETE*/
//Par�metro "e" que necesita la funci�n toma el valor de "this" desde el html y genera objetos de sus etiquetas html, entre ellas, �la id y m�s adelante el value!
function borra(e)
{
    if(confirm('�Est� seguro que desea borrar el registro? Esto no puede ser revertido'))
    {
        var id_boton=e.id;
        var value_boton=e.value;
        //Preparaci�n a un nuevo AJAX
        para_metro='&cmd=BorrarRegistro'
                +'&id_boton=' + id_boton;

        // AJAX
        var xhttp = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
        xhttp.onreadystatechange = function() {
            if(this.readyState == 4 && this.status == 200)      // El servidor procesa la solicitud.
            {
                //document.getElementById('tablaResumenCancha').deleteRow(value_boton);
                alert('Se ha eliminado el registro exitosamente');
                cargaLista();
            }             
            else
                console.log('Si ve este mensaje m�s de 3 veces por env�o, hay un error');
        };
        xhttp.open("POST", "php/command.php", true);    // Tipo de comunicacion
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send(para_metro);  // envio de http
        
        return;
    }
    else
        return false;
    
}