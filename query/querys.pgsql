--CREANDO LA DB
CREATE DATABASE corrego;
--CREANDO TABLAS: CANCHAS
CREATE TABLE canchas(
    id serial not null primary key,
    nombre_cancha varchar(100)
);
--ALGUNAS OCURRENCIAS DE LA TABLA DE LAS CANCHAS
INSERT INTO canchas(nombre_cancha)
VALUES ('Cancha 1'), ('Cancha 2'), ('Cancha 3'),
 ('Cancha 4'), ('Cancha 5');
 --CREANDO LA TABLA DE LAS RESERVAS
 CREATE TABLE reserva_canchas(
    id serial not null primary key,
    nombre varchar(150),
    rut varchar(12),
    email varchar(50),
    fono varchar(12),
    fecha date,
    hora time,
    cancha varchar(50),
    web boolean,
    TV boolean,
    rs boolean,
    amigo boolean
);

--A CONSIDERAR PARA LOS INSERT
/*INSERT INTO reserva_canchas
(nombre,rut,email,fono,fecha,hora,cancha,web,TV,rs,amigo)
VALUES
('Hola Mundo','15468326-9','correo1@mail.com','4569856','23/03/2018','12:12','Cancha 2',false,true,true,true);
*/
--VISTA QUE AGRUPA LA CANTIDAD DE CANCHAS REPETIDAS. SE OMITEN LAS CANCHAS NO ARRENDADAS
--Vista que muestra el consolidado de canchas
DROP VIEW IF EXISTS vw_cuentacanchas CASCADE;
CREATE VIEW vw_cuentacanchas AS
	SELECT cancha, count(*) as CuentaCanchas from reserva_canchas 
	GROUP BY cancha
	ORDER BY CuentaCanchas DESC;
--Vista consolidadora de las canchas
DROP VIEW IF EXISTS vw_estadistica_canchas;
CREATE VIEW vw_estadistica_canchas AS
    --SELECT id, cancha, ROUND(cuentacanchas*100/(select sum(cuentacanchas) from vw_cuentacanchas),0)::numeric AS porcentaje_arriendo from canchas
    SELECT id, cancha, ROUND(cuentacanchas*100/(select sum(cuentacanchas) from vw_cuentacanchas),0) AS porcentaje_arriendo from canchas
    JOIN vw_cuentacanchas AS TablaCuentacanchas
    ON (nombre_cancha=TablaCuentacanchas.cancha)
    ORDER BY porcentaje_arriendo DESC;

--Validaciones a nivel de la base de datos: Correo
DROP FUNCTION IF EXISTS fn_validacorreo_r(character varying) ;
CREATE OR REPLACE FUNCTION fn_validaCorreo_r (varchar) 
RETURNS BOOLEAN AS
	$body$
		DECLARE
		--Declaraci�n de la variable de entrada
		_email ALIAS FOR $1;
		--Declaraci�n de las variables locales de la funci�n
		__esCorreo BOOLEAN;
		BEGIN
			--IF (textregexeq(_email,'^([a-zA-Z])([\w.-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$')=true) THEN
			IF (textregexeq(_email, E'^([a-zA-Z])([a-zA-Z0-9_.-])+\@(([a-zA-Z0-9\-])+([\.]){1})([a-zA-Z0-9]{2,4})+$') = TRUE) THEN
        __esCorreo :=true;
			ELSE
				__esCorreo :=false;
			END IF;
		RETURN __esCorreo;
		END;
	$body$
Language 'plpgsql';

--Ejemplo: select fn_validaCorreo_r('alguien1@gmail.com');

--Validando el RUT
DROP FUNCTION IF EXISTS fn_validarut(VARCHAR) ;
--Funci�n Made in Internet
CREATE OR REPLACE FUNCTION sp_rut_cl(
          rut VARCHAR
          ) RETURNS CHARACTER(1)
   AS
$BODY$
DECLARE
  rec record;
  suma INTEGER := 0;
  serie INTEGER := 2;
  resto INTEGER;
  dv CHARACTER(1);
BEGIN
  --raise notice 'rut: %',rut;
  if (rut is null) then
    return null;
  end if;
  rut := btrim(rut);
  rut := replace(rut, '.', '');
  if (rut is null) then
    return null;
  end if;
  rut := btrim(rut);
  for rec in select * from (
              select substring(rut from i for 1)::char as bit
              from generate_series(length(rut),1,-1) as i
              --where bit = '1'
            ) q1 LOOP
            --raise notice '1';
            --raise notice 'rec.bit: %',rec.bit;
            --raise notice '2';
            if rec.bit is not null and rec.bit ~ '[0-9]+' then
		suma := suma + rec.bit::INTEGER * serie;
            end if;
            --raise notice '3';
            --raise notice 'serie: %',serie;
            if serie = 7 then 
              serie := 1;
            end if;
            serie := serie + 1;
  end loop;
  --raise notice 'suma: %',suma;
  resto := 11 - suma % 11;
  --raise notice 'resto: %',resto;
  dv := case resto when 11 then '0' when 10 then 'K' else resto::CHARACTER end;
  return dv;
end;
$BODY$ LANGUAGE 'plpgsql' volatile;

--Funci�n propia
CREATE OR REPLACE FUNCTION fn_validaRut_r(VARCHAR) RETURNS BOOLEAN AS
$body$
DECLARE
_rut ALIAS FOR $1;
__formatoRUT VARCHAR;
__dv_calculado CHARACTER;
__dv CHARACTER;
__esRut BOOLEAN;
BEGIN
IF (textregexeq(_rut,'^[1-9][0-9]{6,7}[-]([0-9]|[kK])$')=false) THEN
	__esRut=false;
ELSE
	__formatoRUT:=substr(_rut,1,length(_rut)-2);
	__dv := sp_rut_cl(__formatoRUT);
	__dv_calculado := substr(_rut,length(_rut),1);
	IF (__dv = __dv_calculado) THEN
		__esRut= true;
	ELSE
		__esRut= false;
	END IF;
END IF;
RETURN __esRut;
END;
$body$ LANGUAGE 'plpgsql' volatile;

--Ejemplo: select fn_validaRut('15469832-8') da false; pero select fn_validaRut('15469832-9')da true

--Validando nombre
DROP FUNCTION IF EXISTS fn_validaNombre_r(VARCHAR);
CREATE OR REPLACE FUNCTION fn_validaNombre_r(VARCHAR) RETURNS BOOLEAN AS
$body$
DECLARE
_nombre ALIAS FOR $1;
__esNombre BOOLEAN;
BEGIN
IF (textregexeq(_nombre, E'^([a-zA-Z������])+([a-zA-Z������\ ])+$')=TRUE) THEN
	__esNombre=TRUE;
ELSE
	__esNombre=FALSE;
END IF;
RETURN __esNombre;
END;
$body$ LANGUAGE 'plpgsql' volatile;

--Ejemplo: SELECT fn_validaNombre_r('Asyo soy');

--Validando el tel�fono
DROP FUNCTION IF EXISTS fn_validaTelefono_r(VARCHAR);
CREATE OR REPLACE FUNCTION fn_validaTelefono_r(VARCHAR) RETURNS BOOLEAN AS
$body$
DECLARE
_fono ALIAS FOR $1;
__esFono BOOLEAN;
BEGIN
IF (textregexeq(_fono, E'(^[+][1-9]{10,11}[0-9]{9}$)|(^[1-9]{1}[0-9]{6,9}$)|(^[+][1-9]{1}[0-9]{8,9}$)')=TRUE) THEN
--IF (textregexeq(_fono, E'(^[\+]{1}[1-9]{1}[0-9]{7,11}$)|(^[1-9]{1}[0-9]{6,12}$)')=TRUE) THEN
	__esFono=TRUE;
ELSE
	__esFono=FALSE;
END IF;
RETURN __esFono;
END;
$body$ LANGUAGE 'plpgsql' volatile;

--Ejemplo: SELECT fn_validaTelefono_r('120365456');

--Validando cantidad de checkbox
DROP FUNCTION IF EXISTS fn_cuentaCkeck_r(BOOLEAN, BOOLEAN, BOOLEAN, BOOLEAN);
CREATE OR REPLACE FUNCTION fn_cuentaCkeck_r(BOOLEAN, BOOLEAN, BOOLEAN, BOOLEAN) RETURNS INTEGER AS
$body$
DECLARE
_web ALIAS FOR $1;
_rs ALIAS FOR $2;
_TV ALIAS FOR $3;
_amigo ALIAS FOR $4;
__cuentaCkeck INTEGER;
BEGIN
__cuentaCkeck :=0;
IF (_web) THEN
	__cuentaCkeck := __cuentaCkeck +1;
END IF;
IF (_rs) THEN
	__cuentaCkeck := __cuentaCkeck +1;
END IF;
IF (_TV) THEN
	__cuentaCkeck := __cuentaCkeck +1;
END IF;
IF (_amigo) THEN
	__cuentaCkeck := __cuentaCkeck +1;
END IF;
RETURN __cuentaCkeck;
END;
$body$ LANGUAGE 'plpgsql' volatile;

--Ejemplo: SELECT fn_cuentaCkeck_r(FALSE, TRUE, TRUE, TRUE);

--Validaci�n final: Fechas y horas no pueden ser iguales para una misma cancha
DROP FUNCTION IF EXISTS fn_checkArriendo_r(DATE, TIME, VARCHAR(50));
CREATE OR REPLACE FUNCTION fn_checkArriendo_r(DATE, TIME, VARCHAR(50)) RETURNS BOOLEAN AS
$body$
DECLARE
_fecha ALIAS FOR $1;
_hora ALIAS FOR $2;
_cancha ALIAS FOR $3;
__query DATE;
__check BOOLEAN;
BEGIN
__query:=(SELECT fecha FROM reserva_canchas WHERE fecha = _fecha AND hora = _hora AND cancha = _cancha LIMIT 1);
IF (__query is null) THEN
	__check:= TRUE;
ELSE
	__check:=FALSE;
END IF;
RETURN __check;
END;
$body$ LANGUAGE 'plpgsql' volatile;

--Ejemplo: SELECT fn_checkArriendo_r('08/03/2019', '01:22:00', 'Cancha 4'); Es False porque hay registros

--INGRESO FINAL
DROP FUNCTION IF EXISTS fn_ingresoFormulario_i(VARCHAR, VARCHAR, VARCHAR, VARCHAR, DATE, TIME, VARCHAR, BOOLEAN,BOOLEAN,BOOLEAN,BOOLEAN);
--
CREATE OR REPLACE FUNCTION fn_ingresoFormulario_i (VARCHAR, VARCHAR, VARCHAR, VARCHAR, DATE, TIME, VARCHAR, BOOLEAN,BOOLEAN,BOOLEAN,BOOLEAN) 
RETURNS INTEGER AS
$body$
DECLARE
_nombre ALIAS FOR $1;
_rut ALIAS FOR $2;
_correo ALIAS FOR $3;
_fono ALIAS FOR $4;
_fecha ALIAS FOR $5;
_hora ALIAS FOR $6;
_cancha ALIAS FOR $7;
_web ALIAS FOR $8;
_TV ALIAS FOR $9;
_rs ALIAS FOR $10;
_amigo ALIAS FOR $11;
__ckeckRegistro INTEGER;
BEGIN
--Verificaci�n de las validaciones
IF(_nombre IS NULL OR _rut IS NULL OR _correo IS NULL OR _fono IS NULL OR _fecha IS NULL OR _hora IS NULL OR _cancha IS NULL) THEN
	__ckeckRegistro:= -1;
ELSIF (SELECT NOT fn_validaNombre_r(_nombre)) THEN
    __ckeckRegistro:= 1;
ELSIF (SELECT NOT fn_validaRut_r(_rut)) THEN
     __ckeckRegistro:=2;
ELSIF (SELECT NOT fn_validacorreo_r(_correo)) THEN
     __ckeckRegistro:=3;
ELSIF (SELECT NOT fn_validaTelefono_r(_fono)) THEN
     __ckeckRegistro:=4;
ELSIF (SELECT NOT fn_checkArriendo_r(_fecha, _hora, _cancha)) THEN
     __ckeckRegistro:=5;
ELSIF (SELECT fn_cuentaCkeck_r(_web, _TV, _rs, _amigo) < 2) THEN
     __ckeckRegistro:=6;
ELSE
    INSERT INTO reserva_canchas
            (nombre,rut,email,fono,fecha,hora,cancha,web,TV,rs,amigo)
            VALUES
            (_nombre, _rut, _correo, _fono, _fecha,_hora,_cancha,_web,_TV,_rs,_amigo); 
    __ckeckRegistro:=0;
END IF;
RETURN __ckeckRegistro;
END;
$body$ LANGUAGE 'plpgsql';

--EJEMPLO: SELECT fn_ingresoFormulario_i('jOSE JOSE', '15469832-9', 'YOYOYOY@gmail.com', '4445556', '08-03-2019', '01:22:00', 'Cancha 4', TRUE, FALSE, FALSE, true);

--Eliminar registro
DROP FUNCTION IF EXISTS fn_eliminarRegistro_d(INTEGER);
CREATE OR REPLACE FUNCTION fn_eliminarRegistro_d (INTEGER) 
RETURNS VOID AS
$body$
DECLARE
_id ALIAS FOR $1;
BEGIN
	DELETE FROM reserva_canchas WHERE id= _id;
END;
$body$ LANGUAGE 'plpgsql' volatile;

--Ejemplo: SELECT fn_eliminarRegistro_d(34);

